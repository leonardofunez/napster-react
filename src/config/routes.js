import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'

// Globals
import Header from '../components/Header/Header'
import MenuBar from '../components/MenuBar/MenuBar'
import Player from '../components/Player/Player'

// Library
import Dashboard from '../pages/Library/Dashboard/Dashboard'
import Explore from '../pages/Library/Explore/Explore'
import Playlists from '../pages/Library/Playlists/Playlists'

// My Music
import MyPlaylists from '../pages/MyMusic/MyPlaylists/MyPlaylists'
import MyAlbums from '../pages/MyMusic/MyAlbums/MyAlbums'
import MyTracks from '../pages/MyMusic/MyTracks/MyTracks'

// Details
import Artist from '../pages/Details/Artist/Artist'
import Album from '../pages/Details/Album/Album'
import Playlist from '../pages/Details/Playlist/Playlist'
import Genre from '../pages/Details/Genre/Genre'

const routes = (
	<Router>
		<React.Fragment>
			<main className="napster-main is-flex">
					<Route path='/' component={MenuBar}/>
					
					<div className='napster-container is-flex flex-column bg-color-white'>
						<Route path='/' component={Header}/>
						
						<div className='napster-render'>
							<div className='wrapper'>
								{/* Library */}
								<Route path='/' component={Dashboard} exact/>
								<Route path='/explore' component={Explore} exact/>
								<Route path='/playlists' component={Playlists} exact/>

								{/* My Music */}
								<Route path='/my-playlists' component={MyPlaylists} exact/>
								<Route path='/my-albums' component={MyAlbums} exact/>
								<Route path='/my-tracks' component={MyTracks} exact/>

								{/* Details */}
								<Route path='/artists/:id' component={Artist} exact/>
								<Route path='/albums/:id' component={Album} exact/>
								<Route path='/playlists/:id' component={Playlist} exact/>
								<Route path='/genres/:id' component={Genre} exact/>
							</div>
						</div>
					</div>

				<Route path='/' component={Player}/>
			</main>
		</React.Fragment>
	</Router>
)

export default routes;