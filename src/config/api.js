const api_key = 'YTkxZTRhNzAtODdlNy00ZjMzLTg0MWItOTc0NmZmNjU4Yzk4'

const apiNapster = {
    baseURL: `https://api.napster.com/v2.2`,
    imageURL: `http://direct.napster.com/imageserver`,
    withCredentials: false,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
}

export default{
  // Images
    getImage(type, id, size){
      return type !== 'genre' ? `${apiNapster.imageURL}/v2/${type}/${id}/images/${size}.jpg` : `${apiNapster.imageURL}/images/${id}/${size}.jpg`
    },


  // Pages
    // Dashboard
      async getTopAlbums(limit){
        try {
          const response = await fetch(`${apiNapster.baseURL}/albums/top?limit=${limit}&apikey=${api_key}`)
          return await response.json()
        }
        catch (error) {
          console.error('Error:', error)
        }
      },

      async getNewReleases(limit){
        try {
          const response = await fetch(`${apiNapster.baseURL}/albums/new?limit=${limit}&apikey=${api_key}`);
          return await response.json()
        }
        catch (error) {
          console.error('Error:', error)
        }
      },

      async getTopTracks(limit){
        try {
          const response = await fetch(`${apiNapster.baseURL}/tracks/top?limit=${limit}&apikey=${api_key}`);
          return await response.json()
        }
        catch (error) {
          console.error('Error:', error)
        }
      },

      async getGenres(limit){
        try {
          const response = await fetch(`${apiNapster.baseURL}/genres?limit=${limit}&apikey=${api_key}`);
          return await response.json()
        }
        catch (error) {
          console.error('Error:', error)
        }
      },

    // Explore
      async getStaffAlbums(limit){
        try {
          const response = await fetch(`${apiNapster.baseURL}/albums/picks?limit=${limit}&apikey=${api_key}`);
          return await response.json()
        }
        catch (error) {
          console.error('Error:', error)
        }
      },

      async getTopPlaylists(limit){
        try {
          const response = await fetch(`${apiNapster.baseURL}/playlists/featured?limit=${limit}&apikey=${api_key}`);
          return await response.json()
        }
        catch (error) {
          console.error('Error:', error)
        }
      },

      async getTopArtists(limit){
        try {
          const response = await fetch(`${apiNapster.baseURL}/artists/top?limit=${limit}&apikey=${api_key}`);
          return await response.json()
        }
        catch (error) {
          console.error('Error:', error)
        }
      },

    // Playlists
      async getFeaturedPlaylists(limit){
        try {
          const response = await fetch(`${apiNapster.baseURL}/playlists/featured?limit=${limit}&apikey=${api_key}`);
          return await response.json()
        }
        catch (error) {
          console.error('Error:', error)
        }
      },

      async getMorePlaylists(limit, offset){
        try {
          const response = await fetch(`${apiNapster.baseURL}/playlists?limit=${limit}&offset=${offset}&apikey=${api_key}`)
          return await response.json()
        }
        catch (error) {
          console.error('Error:', error)
        }
      },
  // .Pages

  // Details
    // Album
        async getAlbumDetail(album_id){
          try {
            const response = await fetch(`${apiNapster.baseURL}/albums/${album_id}?apikey=${api_key}`)
            return await response.json()
          }
          catch (error) {
            console.error('Error:', error)
          }
        },

        async getFeaturedAlbums(){
          try {
            const response = await fetch(`${apiNapster.baseURL}/albums/top?limit=5&apikey=${api_key}`)
            return await response.json()
          }
          catch (error) {
            console.error('Error:', error)
          }
        },

    // Artist
        async getArtistDetail(artist_id){
          try {
            const response = await fetch(`${apiNapster.baseURL}/artists/${artist_id}?apikey=${api_key}`)
            return await response.json()
          }
          catch (error) {
            console.error('Error:', error)
          }
        },

        async getArtistAlbums(artist_id){
          try {
            const response = await fetch(`${apiNapster.baseURL}/artists/${artist_id}/albums?apikey=${api_key}`)
            return await response.json()
          }
          catch (error) {
            console.error('Error:', error)
          }
        },

    // Playlist
        async getPlaylistDetail(playlist_id){
          try {
            const response = await fetch(`${apiNapster.baseURL}/playlists/${playlist_id}?apikey=${api_key}`)
            return await response.json()
          }
          catch (error) {
            console.error('Error:', error)
          }
        },

        async getFeaturedPlayists(){
          try {
            const response = await fetch(`${apiNapster.baseURL}/playlists/featured?limit=5&apikey=${api_key}`)
            return await response.json()
          }
          catch (error) {
            console.error('Error:', error)
          }
        },

    // Genre
        async getGenreDetail(genre_id){
          try {
            const response = await fetch(`${apiNapster.baseURL}/genres/${genre_id}?apikey=${api_key}`)
            return await response.json()
          }catch (error) {
            console.log('Error:', error)
          }
        },

        async getGenreTopPlaylists(genre_id){
          try{
            const response = await fetch(`${apiNapster.baseURL}/genres/${genre_id}/playlists/top?apikey=${api_key}`)
            return await response.json()
          }catch (error) {
            console.log('Error: ', error)
          }
        },

        async getGenreTopArtists(genre_id){
          try {
            const response = await fetch(`${apiNapster.baseURL}/genres/${genre_id}/artists/top?apikey=${api_key}`)
            return await response.json()
          }catch (error) {
            console.log('Error:', error)
          }
        },
  // .Details
  
  // Tracks
    // Album
        async getAlbumTracks(album_id){
          try {
            const response = await fetch(`${apiNapster.baseURL}/albums/${album_id}/tracks?apikey=${api_key}`)
            return await response.json()
          }catch( error ){
            console.log('Error:', error)
          }
        },

    // Playlist
        async getPlaylistTracks(playlist_id){
          try {
            const response = await fetch(`${apiNapster.baseURL}/playlists/${playlist_id}/tracks?apikey=${api_key}&limit=100`)
            return await response.json()
          }catch( error ){
            console.log('Error:', error)
          }
        },
  // .Tracks
          
  // // Singles
  //   // Track
  //     getTrack(track_id){
  //         return apiNapster(`/tracks/${track_id}?apikey=${api_key}`)
  //     },
    
  //   // Album
  //     getAlbum(album_id){
  //         return apiNapster(`/albums/${album_id}?apikey=${api_key}`)
  //     },
    
  //   // Playlist
  //     getPlaylist(playlist_id){
  //         return apiNapster(`/playlists/${playlist_id}?apikey=${api_key}`)
  //     },
  // // .Singles
}