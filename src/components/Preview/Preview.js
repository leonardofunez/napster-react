import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import './Preview.scss'

export default class Preview extends Component{
  constructor() {
    super();

    this.state = {
      type: 'medium'
    }
  }

  render (){
    const previewStyle = {
      background: `url(${this.props.photo}) no-repeat center / cover #9B9FA7`
    }

    const previewInfo = (
      <div className='preview__info'>
        <h2 className='preview__title ellipsis f-weight-500'>{this.props.title}</h2>
        <small className='preview__subtitle f-weight-500'>{this.props.subtitle}</small>
      </div>
    )

    const previewClass = `preview preview--${this.props.type}`

    return(
      <React.Fragment>
        <div className={previewClass}>
          <NavLink to={this.props.url}>
            <div className='preview__photo is-relative' style={previewStyle}>
              { this.props.type !== 'small' ? <span className='preview__like is-absolute like'></span> : null }
              { this.props.type !== 'medium' ? previewInfo : null }
            </div>
            { this.props.type === 'medium' ? previewInfo : null }
          </NavLink>
        </div>
      </React.Fragment>
    );
  }
}