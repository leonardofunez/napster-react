import React, { Component } from 'react'
import './Header.scss'
import profile from '../../assets/img/test/profile.png'

export default class Header extends Component {
  render() {
    const profileStyle = {
      background: `url(${profile}) no-repeat center / cover #3900f1`
    }

    return(
      <header className='main-header'>
        <div className='wrapper is-flex flex-between flex-align-center'>
          <h1 className='main-header__title color-blue'>
            {this.state.page_name}
          </h1>

          <div className='main-header__options is-flex'>
            <div className='main-header__notifications'></div>
            <div className='main-header__search'></div>
            <div className='main-header__profile is-flex flex-align-center'>
              <div className='main-header__profile-name'>{this.state.name}</div>
              <div className='main-header__profile-photo bg-color-gray' style={profileStyle}></div>
              <div className='main-header__profile-menu is-relative'>
                <div className='main-header__menu f-14 bg-color-white is-absolute b-radius-4'>
                  <div className='main-header__menu-item'>Account settings</div>
                  <div className='main-header__menu-item'>Keyboard shortcuts</div>
                  <div className='main-header__menu-item'>Video tutorials</div>
                  <div className='main-header__menu-item'>Logout</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }

  constructor() {
    super();

    this.state = {
      page_name: 'Dashboard',
      uid: 1,
      name: 'Leonardo Funez',
      photo: '',
    }
  }
}