import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import './TrackItem.scss'

export default class TrackItem extends Component {
  render(){
    return(
      <tr className="track-item is-flex flex-align-center bg-color-gray b-radius-4 f-14">
        <td className='track-item__play play'></td>
        <td className='track-item__title ellipsis'>{this.props.title}</td>
        <td className='track-item__album ellipsis'>
          { this.props.albumId !== '' ?
            <NavLink to={`/albums/${this.props.albumId}`} className='color-dark'>{this.props.album}</NavLink>
          :
            <NavLink to={`/artists/${this.props.artistId}`} className='color-dark'>{this.props.artist}</NavLink>
          }
        </td>
        <td className='track-item__like like like--dark' onClick={() => this.likeTrack(this.props.trackId)}></td>
        <td className='track-item__duration'>{this.getTrackTime(this.props.duration)}</td>
      </tr>
    );
  }

  getTrackTime(time) {
    let hr = ~~(time / 3600)
    let min = ~~((time % 3600) / 60)
    let sec = time % 60
    let sec_min = ""
    let hrs
    if (hr > 0) {
        sec_min += "" + hrs + ":" + (min < 10 ? "0" : "");
    }
    sec_min += "" + min + ":" + (sec < 10 ? "0" : "");
    sec_min += "" + sec;
    return sec_min;
  }

  likeTrack(track_id){
    console.log(track_id);
  }
}