import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import './MenuBar.scss'

export default class MenuBar extends Component {
  render() {
    return(
      <aside className='main-menu bg-color-dark is-flex flex-column flex-justify-center is-relative'>
        <NavLink to={'/'} className='main-menu__logo is-block m-auto is-absolute'></NavLink>

        <div className='main-menu__items'>
          <div className='main-menu__title color-blue'>Library</div>
          <NavLink to={'/'} className='main-menu__item main-menu__item--dashboard'>Dashboard</NavLink>
          <NavLink to={'/explore'} className='main-menu__item main-menu__item--explore'>Explore</NavLink>
          <NavLink to={'/playlists'} className='main-menu__item main-menu__item--playlists main-menu__item--last'>Playlists</NavLink>
          
          <div className='main-menu__title color-blue'>My Music</div>
          <NavLink to={'/my-playlists'} className='main-menu__item main-menu__item--my-playlists'>Playlists</NavLink>
          <NavLink to={'/my-albums'} className='main-menu__item main-menu__item--my-albums'>Albums</NavLink>
          <NavLink to={'/my-tracks'} className='main-menu__item main-menu__item--my-tracks'>Tracks</NavLink>
        </div>
      </aside>
    );
  }
}