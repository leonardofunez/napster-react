import React, { Component } from 'react'
import './Player.scss'
import photo from '../../assets/img/test/album.jpg'

export default class Player extends Component {
  render() {
    const albumStyle = {
      background: `url(${photo}) no-repeat center / cover #3900f1`
    }

    return(
      <div className='main-player bg-gradient-blue w-100 is-absolute to-bottom is-flex flex-between'>
        <div className='main-player__current-song is-flex flex-align-center'>
          <div className='main-player__photo b-radius-4' style={albumStyle}></div>
          <div className='main-player__song-name-artist is-flex flex-column'>
            <div className='main-player__song color-green f-weight-600 f-14'>{this.state.song}</div>
            <div className='main-player__artist color-white f-12'>{this.state.artist}</div>
          </div>
        </div>

        <div className='main-player__controls is-flex flex-column flex-align-center'>
          <div className='main-player__progress-bar w-100 is-flex flex-align-center'>
            <span className='main-player__time color-white f-12 f-weight-500'>{this.state.track_duration}</span>
            <input
                type="range" 
                id="progress_control"
                min="1"
                max="10"
                value={this.state.currentTime}
                onChange={(e) => this.changeTime(e)}
                className="main-player__range-time slider"
            />
            <span className='main-player__time color-white f-12 f-weight-500'>{this.state.track_current_time}</span>
          </div>

          <div className='main-player__buttons is-flex flex-align-center'>
            <div className='main-player__shuffle is-relative'></div>
            <div className='main-player__previous'></div>
            <div className='main-player__play'></div>
            <div className='main-player__next'></div>
            <div className='main-player__repeat is-relative'></div>
          </div>
        </div>

        <div className='main-player__volume is-flex flex-align-center'>
          <div className='main-player__volume-button'></div>
          <input
              type="range" 
              id="volume_control"
              min="1"
              max="10"
              value={this.state.volume}
              onChange={(e) => this.changeVolume(e)}
              className="main-player__range-volume slider"
          />
        </div>
      </div>
    );
  }

  constructor() {
    super();

    this.state = {
      id: 1,
      photo: '../../assets/img/test/album.jpg',
      song: 'The heart speaks in whispers',
      artist: 'Corinna Bailey Rae',

      track: new Audio(),
      track_isnew: true,
      track_src: '',
      track_duration: '0:00',
      track_current_time: '0:00',

      currentTime: 0,
      volume: 5,
      mute: false
    }
  }

  changeTime(e){
    this.setState({
      currentTime: e.target.value
    })
  }

  changeVolume(e){
    this.setState({
      volume: e.target.value
    })
    // this.state.track.volume = e.target.value / 10
  }

  mute() {
    this.setState({
      mute: !this.state.mute
    })
    // this.state.track.muted = !this.state.mute
  }
}