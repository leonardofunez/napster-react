import React, { Component } from 'react'
import './MyPlaylists.scss'

export default class MyPlaylists extends Component {
  render() {
    return(
      <div className='napster-page napster-page__my-playlists'>
        My Playlists
      </div>
    );
  }
}