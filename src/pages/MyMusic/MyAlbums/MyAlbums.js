import React, { Component } from 'react'
import './MyAlbums.scss'

export default class MyAlbums extends Component {
  render() {
    return(
      <div className='napster-page napster-page__my-albums'>
        My Albums
      </div>
    );
  }
}