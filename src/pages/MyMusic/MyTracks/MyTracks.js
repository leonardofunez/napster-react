import React, { Component } from 'react'
import './MyTracks.scss'

export default class MyTracks extends Component {
  render() {
    return(
      <div className='napster-page napster-page__my-tracks'>
        My Tracks
      </div>
    );
  }
}