import React, { Component } from 'react'
import NapsterAPI from '../../../config/api'

import Preview from '../../../components/Preview/Preview'
import TrackItem from '../../../components/TrackItem/TrackItem'

export default class Dashboard extends Component {
  render(){
    return(
      <div className="napster-page napster-page__dashboard">
        {/* Top Albums */}
          <div className='napster-page__top-albums'>
            <h2 className='napster-page__block-title f-weight-400'>Top Albums</h2>
            <div className='is-flex flex-wrap'>
              { this.state.topAlbums.map( album => 
                <Preview
                  key={album.id}
                  type='big'
                  title={album.name}
                  subtitle={album.artistName}
                  photo={this.getImage('albums', album.id, '500x500')}
                  url={'/albums/' + album.id}
                />
              )}
            </div>
          </div>
        {/* .Top Albums */}
        
        {/* New Releases */}
          <div className='napster-page napster-page__new-releases'>
            <h2 className='napster-page__block-title f-weight-400'>New Releases</h2>
            <div className='is-flex flex-wrap'>
              { this.state.newReleases.map( album => 
                <Preview
                  key={album.id}
                  type='medium'
                  title={album.name}
                  subtitle={album.artistName}
                  photo={this.getImage('albums', album.id, '500x500')}
                  url={'/albums/' + album.id}
                />
              )}
            </div>
          </div>
        {/* .New Releases */}

        {/* Top Tracks */}
          <div className='napster-page napster-page__top-tracks'>
            <h2 className='napster-page__block-title f-weight-400'>Top Tracks</h2> 
            <table cellPadding='0' cellSpacing='0' className='w-100'>
              <thead>
                <tr className='tracks-head is-flex flex-align-center'>
                  <td className='tracks-head__play'></td>
                  <td className='tracks-head__title'>Title</td>
                  <td className='tracks-head__album'>Artist</td>
                  <td className='tracks-head__like'>Like</td>
                  <td className='tracks-head__duration'>Duration</td>
                </tr>
              </thead>
              <tbody className='napster-detail__track-list'>
                { this.state.topTracks.map( track =>
                  <TrackItem
                    key={track.id}
                    trackId={track.id}
                    title={track.name}
                    album=''
                    albumId=''
                    artist={track.artistName}
                    artistId={track.artistId}
                    duration={track.playbackSeconds}
                  />
                )} 
              </tbody>
            </table>
          </div>
        {/* .Top Tracks */}

        {/* Genres */}
          <div className='napster-page napster-page__genres'>
            <h2 className='napster-page__block-title f-weight-400'>Genres</h2>
            <div className='napster-page__small-container is-flex flex-wrap'>
              { this.state.genres.map( genre =>
                <Preview
                  key={genre.id}
                  type='small'
                  title={genre.name}
                  subtitle=''
                  photo={this.getImage('genre', genre.id, '240x160')}
                  url={'/genres/' + genre.id} />
              )}   
            </div>
          </div>
        {/* .Genres */}
      </div>
    );
  }

  constructor() {
    super();

    this.state = {
      topAlbums : [],
      newReleases: [],
      topTracks: [],
      genres: []
    }
  }

  componentDidMount(){
    this.getTopAlbums()
    this.getNewReleases()
    this.getTopTracks()
    this.getGenres()
  }

  async getTopAlbums(){
    const topAlbums = await NapsterAPI.getTopAlbums(3)
    this.setState({ topAlbums:topAlbums.albums })
    // console.log(this.state.topAlbums)
  }

  async getNewReleases(){
    const newReleases = await NapsterAPI.getNewReleases(12)
    this.setState({ newReleases:newReleases.albums })
    // console.log(this.state.newReleases)
  }

  async getTopTracks(){
    const topTracks = await NapsterAPI.getTopTracks(10)
    this.setState({ topTracks:topTracks.tracks })
    // console.log(this.state.topTracks)
  }

  async getGenres(){
    const genres = await NapsterAPI.getGenres()
    this.setState({ genres:genres.genres })
    // console.log(this.state.genres)
  }

  getImage(type, id, size){
    return NapsterAPI.getImage(type, id, size)
  }
}