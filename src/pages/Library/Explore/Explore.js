import React, { Component } from 'react'
import NapsterAPI from '../../../config/api'

import Preview from '../../../components/Preview/Preview'

export default class Explore extends Component {
  render(){
    return(
      <div className="napster-page napster-page__dashboard">
        {/* Staff Albums */}
          <div className='napster-page__top-albums'>
            <h2 className='napster-page__block-title f-weight-400'>Staff Albums</h2>
            <div className='is-flex flex-wrap'>
              { this.state.staffAlbums.map( album => 
                <Preview
                  key={album.id}
                  type='big'
                  title={album.name}
                  subtitle={album.artistName}
                  photo={this.getImage('albums', album.id, '500x500')}
                  url={'/albums/' + album.id}
                />
              )}
            </div>
          </div>
        {/* .Staff Albums */}
        
        {/* Top Playlists */}
          <div className='napster-page napster-page__new-releases'>
            <h2 className='napster-page__block-title f-weight-400'>Top Playlists</h2>
            <div className='is-flex flex-wrap'>
              { this.state.topPlaylists.map( playlist => 
                <Preview
                  key={playlist.id}
                  type='medium'
                  title={playlist.name}
                  subtitle={playlist.trackCount + ' Tracks'}
                  photo={playlist.images[0].url}
                  url={'/playlists/' + playlist.id}
                />
              )}
            </div>
          </div>
        {/* .Top Playlists */}

        {/* Top Artists */}
          <div className='napster-page napster-page__top-tracks'>
            <h2 className='napster-page__block-title f-weight-400'>Top Artists</h2>
            <div className='napster-page__small-container is-flex flex-wrap'>
              { this.state.topArtists.map( artist =>
                <Preview
                  key={artist.id}
                  type='small'
                  title={artist.name}
                  subtitle=''
                  photo={this.getImage('artists', artist.id, '240x160')}
                  url={'/artists/' + artist.id}
                />
              )}   
            </div>    
          </div>
        {/* .Top Artists */}
      </div>
    );
  }

  constructor() {
    super();

    this.state = {
      staffAlbums : [],
      topPlaylists: [],
      topArtists: [],
    }
  }

  componentDidMount(){
    this.getStaffAlbums()
    this.getTopPlaylists()
    this.getTopArtists()
  }

  async getStaffAlbums(){
    const staffAlbums = await NapsterAPI.getStaffAlbums(3)
    this.setState({ staffAlbums : staffAlbums.albums })
  }

  async getTopPlaylists(){
    const topPlaylists = await NapsterAPI.getTopPlaylists(16)
    this.setState({ topPlaylists : topPlaylists.playlists })
  }

  async getTopArtists(){
    const topArtists = await NapsterAPI.getTopArtists(20)
    this.setState({ topArtists:topArtists.artists })
  }

  getImage(type, id, size){
    return NapsterAPI.getImage(type, id, size)
  }
}