import React, { Component } from 'react'
import NapsterAPI from '../../../config/api'

import Preview from '../../../components/Preview/Preview'

export default class Playlists extends Component {
  render(){
    return(
      <div className="napster-page napster-page__dashboard">
        {/* Feature Playlists */}
          <div className='napster-page__top-albums'>
            <h2 className='napster-page__block-title f-weight-400'>Feature Playlists</h2>
            <div className='is-flex flex-wrap'>
              { this.state.featuredPlaylists.map( playlist => 
                <Preview
                  key={playlist.id}
                  type='big'
                  title={playlist.name}
                  subtitle={playlist.trackCount + ' Tracks'}
                  photo={playlist.images[0].url}
                  url={'/playlists/' + playlist.id}
                />
              )}
            </div>
          </div>
        {/* .Feature Playlists */}
        
        {/* More Playlists */}
          <div className='napster-page napster-page__new-releases'>
            <h2 className='napster-page__block-title f-weight-400'>More Playlists</h2>
            <div className='is-flex flex-wrap'>
              { this.state.morePlaylists.map( playlist => 
                <Preview
                  key={playlist.id}
                  type='medium'
                  title={playlist.name}
                  subtitle={playlist.trackCount + ' Tracks'}
                  photo={playlist.images[0].url}
                  url={'/playlists/' + playlist.id}
                />
              )}
            </div>
          </div>
        {/* .More Playlists */}
      </div>
    );
  }

  constructor() {
    super();

    this.state = {
      featuredPlaylists: [],
      morePlaylists: []
    }
  }

  componentDidMount(){
    this.getFeaturedPlaylists()
    this.getMorePlaylists()
  }

  async getFeaturedPlaylists(){
    const featuredPlaylists = await NapsterAPI.getFeaturedPlaylists(3)
    this.setState({ featuredPlaylists : featuredPlaylists.playlists })
  }

  async getMorePlaylists(){
    const morePlaylists = await NapsterAPI.getMorePlaylists(16, 3)
    this.setState({ morePlaylists : morePlaylists.playlists })
  }

  getImage(type, id, size){
    return NapsterAPI.getImage(type, id, size)
  }
}