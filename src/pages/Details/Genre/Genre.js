import React, { Component } from 'react'
import NapsterAPI from '../../../config/api'
import Preview from '../../../components/Preview/Preview'

export default class Genre extends Component {
  render(){
    return(
      <div className='napster-page napster-page__Genre'>
        <div className='napster-detail'>
          <div className='napster-detail__top is-flex'>
            <h2 className='napster-detail__big-title ellipsis'>{this.state.genre.name}</h2>
          </div>
          
          <div className='divider'></div>
          
          {/* Top Playlists */}
            <div className='napster-page napster-page__new-releases'>
              <h2 className='napster-page__block-title f-weight-400'>Top Playlists</h2>
              <div className='is-flex flex-wrap'>
                { this.state.playlists.map( playlist => 
                  <Preview
                    key={playlist.id}
                    type='medium'
                    title={playlist.name}
                    subtitle={playlist.trackCount + ' Tracks'}
                    photo={playlist.images[0].url}
                    url={'/playlists/' + playlist.id}
                  />
                )}
              </div>
            </div>
          {/* .Top Playlists */}

          {/* Top Artists */}
            <div className='napster-page napster-page__top-tracks'>
              <h2 className='napster-page__block-title f-weight-400'>Top Artists</h2>
              <div className='napster-page__small-container is-flex flex-wrap'>
                { this.state.artists.map( artist =>
                  <Preview
                    key={artist.id}
                    type='small'
                    title={artist.name}
                    subtitle=''
                    photo={this.getImage('artists', artist.id, '240x160')}
                    url={'/artists/' + artist.id}
                  />
                )}   
              </div>    
            </div>
          {/* .Top Artists */}
        </div>
      </div>
    );
  }

  constructor() {
    super();

    this.state = {
      genre : {},
      playlists: [],
      artists: []
    }
  }

  componentDidMount(){
    this.getGenreDetail()
    this.getGenreTopPlaylists()
    this.getGenreTopArtists()
  }

  async getGenreDetail(){
    const response = await NapsterAPI.getGenreDetail(this.props.match.params.id)
    console.log(response.genres[0])
    this.setState({ genre : response.genres[0] })
  }

  async getGenreTopPlaylists(genre_id){
    const response = await NapsterAPI.getGenreTopPlaylists(this.props.match.params.id)
    this.setState({ playlists : response.playlists })
  }

  async getGenreTopArtists(genre_id){
    const response = await NapsterAPI.getGenreTopArtists(this.props.match.params.id)
    this.setState({ artists : response.artists })
  }

  getImage(type, id, size){
    return NapsterAPI.getImage(type, id, size)
  }
}