import React, { Component } from 'react'
import NapsterAPI from '../../../config/api'
import TrackItem from '../../../components/TrackItem/TrackItem'

export default class Playlist extends Component {
  render(){
    const photoStyle = {
      background: `url(${this.getImage()}) no-repeat center / cover #9B9FA7`
    }

    return(
      <div className='napster-page napster-page__playlist'>
        <div className='napster-detail'>
          {/* Top Information */}
            <div className='napster-detail__top is-flex'>
              <div className='napster-detail__photo b-radius-4' style={photoStyle}></div>
              
              <div className='napster-detail__info w-100'>
                <div className='is-flex flex-column'>
                  <h2 className='napster-detail__title f-18'>{this.state.playlist.name}</h2>
                  <span className='napster-detail__subtitle f-14'>{this.state.playlist.trackCount + ' Tracks'}</span>
                </div>
                <div className='napster-detail__controls is-flex'>
                  <div className='napster-detail__play-button is-flex flex-align-center bg-color-gray b-radius-4'>
                    <span className='play'></span>
                    <span className='f-14'>{this.state.play_state}</span>
                  </div>
                  <div className='napster-detail__filter bg-color-gray b-radius-4'>
                    <input type='search' placeholder='Filter...' className='f-14'></input>
                  </div>
                  <div className='napster-detail__like bg-color-gray b-radius-4' onClick={() => this.likePlaylist(this.state.playlist.id)}>
                    <span className='like like--dark'></span>
                  </div>
                </div>
              </div>
            </div>
          {/* .Top Information */}

          <div className='divider'></div>

          {/* Track List */}
            <table cellPadding='0' cellSpacing='0' className='w-100'>
              <thead>
                <tr className='tracks-head is-flex flex-align-center'>
                  <td className='tracks-head__play'></td>
                  <td className='tracks-head__title'>Title</td>
                  <td className='tracks-head__album'>Artist</td>
                  <td className='tracks-head__like'>Like</td>
                  <td className='tracks-head__duration'>Duration</td>
                </tr>
              </thead>
              <tbody className='napster-detail__track-list'>
                { this.state.track_list.map( track =>
                  <TrackItem
                    key={track.id}
                    trackId={track.id}
                    title={track.name}
                    album=''
                    albumId=''
                    artist={track.artistName}
                    artistId={track.artistId}
                    duration={track.playbackSeconds}
                  />
                )}  
              </tbody>
            </table>
          {/* .Track List */}
        </div>
      </div>
    );
  }

  constructor() {
    super();

    this.state = {
      playlist : {},
      playlist_photo: '',
      track_list: [],
      play_state: 'Play all'
    }
  }

  componentDidMount(){
    this.getPlaylistDetail()
    this.getPlaylistTracks()
  }

  async getPlaylistDetail(){
    const response = await NapsterAPI.getPlaylistDetail(this.props.match.params.id)
    this.setState({ 
      playlist : response.playlists[0],
      playlist_photo : response.playlists[0].images[0].url
    })
  }

  async getPlaylistTracks(){
    const response = await NapsterAPI.getPlaylistTracks(this.props.match.params.id)
    this.setState({ track_list : response.tracks })
  }

  getImage = () => this.state.playlist_photo

  likePlaylist(playlist_id){
    console.log(playlist_id)
  }
}