import React, { Component } from 'react'
import NapsterAPI from '../../../config/api'

import Preview from '../../../components/Preview/Preview'

export default class Artist extends Component {
  render(){
    return(
      <div className="napster-page napster-page__artist">
        <div className='napster-detail'>
          {/* Top Information */}
            <div className='napster-detail__top is-flex'>
              <div className='napster-detail__info w-100'>
                <div className='is-flex flex-align-center'>
                  <div>
                    <h2 className='napster-detail__big-title'>{this.state.artist.name}</h2>
                    {/* <span className='napster-detail__subtitle'>{this.state.albums.length + ' Albums'}</span> */}
                  </div>
                  <div className='napster-detail__like bg-color-gray b-radius-4' onClick={() => this.likeArtist(this.state.artist.id)}>
                    <span className='like like--dark'></span>
                  </div>
                </div>
              </div>
            </div>
          {/* .Top Information */}

          <div className='divider'></div>

          {/* Album List */}
            <div className='is-flex flex-wrap'>
              { this.state.albums.map( album => 
                <Preview
                  key={album.id}
                  type='medium'
                  title={album.name}
                  subtitle={album.trackCount + ' Tracks'}
                  photo={this.getAlbumImage(album.id)}
                  url={'/albums/' + album.id}
                />
              )}
            </div>
          {/* .Album List */}
        </div>
      </div>
    );
  }

  constructor() {
    super();

    this.state = {
      artist : {},
      albums: []
    }
  }

  componentDidMount(){
    this.getArtistDetail()
    this.getArtistAlbums()
  }

  async getArtistDetail(){
    const response = await NapsterAPI.getArtistDetail(this.props.match.params.id)
    this.setState({ artist : response.artists[0] })
  }

  async getArtistAlbums(){
    const response = await NapsterAPI.getArtistAlbums(this.props.match.params.id)
    this.setState({ albums : response.albums })
  }

  getArtistImage(){
    return NapsterAPI.getImage('artists', this.state.artist.id, '240x160')
  }

  getAlbumImage(album_id){
    return NapsterAPI.getImage('albums', album_id, '500x500')
  }

  likeArtist(artist_id){
    console.log(artist_id)
  }
}